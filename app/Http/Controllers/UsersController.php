<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class UsersController extends Controller
{
    public  function index(){
        $users=[
            '0'=>[
                'first_Name'=>'Pallab',
                'Last_Name'=>'Roy',
                'Location' =>'Bangladesh'
            ],

            '2'=>[
                'first_Name'=>'Akhy',
                'Last_Name'=>'Hk',
                'Location' =>'India'
            ]
        ];
        return view('Admin.users.index', compact('users'));

    }
    public  function create(){
        return view('Admin.users.create');
    }

    public function  store(Request $request){
        User::create($request->all());
        return 'success';
        return $request->all();
    }
}
?>